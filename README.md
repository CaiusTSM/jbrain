Overview:
jBrain the high level artificial intelligence library written in java. jBrain's main goal is to be able to solve as many different problems as possible while not taking too long to do so. Another main goal of the library is too require as little code as possible from the library user.

How it works:
jBrain uses feed forward neural networks and various learning algorithms to accomplish its goals.
...will write more in future...

License:
This project is under the zLib license.

Copyright (c) <'2014'> <'Lucas Laukien'>

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.