package NeuralNetwork;

public class Neuron extends Node
{	
	public Synapse[] m_synapses;
	
	public float m_bias;
	public float m_prevDBias = 0.0f;
	
	public float m_error;
	
	/**
	 * Sigmoid function.
	 * @param x
	 * @return y
	 */
	private static float sigmoid(float x)
	{
		return 1.0f / (1.0f + (float)Math.exp(-x));
	}
	
	/**
	 * Calculates the neuron's output.
	 * @param activationMultiplier Magnitude of activation.
	 */
	public void update(float activationMultiplier)
	{
		float sum = m_bias;
		for (int i = 0; i < m_synapses.length; i++)
			sum += m_synapses[i].m_node.m_output * m_synapses[i].m_weight;
		m_output = sigmoid(sum * activationMultiplier);
	}
	
	/**
	 * Calculates the neuron's outputs linearly.
	 * @param activationMultiplier Magnitude of activation.
	 */
	public void updateLinear(float activationMultiplier)
	{
		float sum = m_bias;
		for (int i = 0; i < m_synapses.length; i++)
			sum += m_synapses[i].m_node.m_output * m_synapses[i].m_weight;
		m_output = sum * activationMultiplier;
	}
	
	@Override
	public String toString()
	{
		String ret = "" + m_bias;
		
		for (int i = 0; i < m_synapses.length; i++)
			ret += " " + m_synapses[i].m_weight;
		
		return ret;
	}
}
