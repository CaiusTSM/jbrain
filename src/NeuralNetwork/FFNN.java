package NeuralNetwork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

import Util.Util;

public class FFNN
{	
	private Node[] m_inputs;
	private Neuron[][] m_hidden;
	private Neuron[] m_outputs;
	
	private int m_numInputs;
	private int m_numOutputs;
	
	private int m_numHidden;
	private int m_numPerHidden;
	
	private float m_minWeight;
	private float m_maxWeight;
	
	public float m_momentum = 0.0f;
	
	public float m_activationMultiplier = 1.0f;
	
	/**
	 * Constructor.
	 * @param numInputs Number of inputs.
	 * @param numOutputs Number of outputs.
	 * @param numHidden Number of hidden layers.
	 * @param numPerHidden Number of neurons per hidden layer.
	 * @param minWeight Minimum possible weight during construction of synapses.
	 * @param maxWeight Maximum possible weight during construction of synapses.
	 */
	public FFNN(int numInputs, int numOutputs, int numHidden, int numPerHidden, float minWeight, float maxWeight)
	{
		m_inputs = new Node[numInputs];
		
		for (int i = 0; i < m_inputs.length; i++)
			m_inputs[i] = new Node();
		
		if (numHidden > 0)
		{
			m_hidden = new Neuron[numHidden][numPerHidden];
			
			//first layer connected to input layer
			m_hidden[0] = new Neuron[numPerHidden];
			
			for (int i = 0; i < m_hidden[0].length; i++)
			{
				m_hidden[0][i] = new Neuron();
				
				m_hidden[0][i].m_synapses = new Synapse[numInputs];
				
				m_hidden[0][i].m_bias = Util.getRandom(minWeight, maxWeight);
				
				for (int j = 0; j < m_hidden[0][i].m_synapses.length; j++)
				{
					m_hidden[0][i].m_synapses[j] = new Synapse();
					
					m_hidden[0][i].m_synapses[j].m_node = m_inputs[j];
					
					m_hidden[0][i].m_synapses[j].m_weight = Util.getRandom(minWeight, maxWeight);
				}
			}
			
			//in-between hidden layers
			for (int i = 1; i < m_hidden.length; i++)
			{
				m_hidden[i] = new Neuron[numPerHidden];
				
				for (int j = 0; j < m_hidden[i].length; j++)
				{
					m_hidden[i][j] = new Neuron();
					
					m_hidden[i][j].m_synapses = new Synapse[m_hidden[i - 1].length];
					
					m_hidden[i][j].m_bias = Util.getRandom(minWeight, maxWeight);
					
					for (int k = 0; k < m_hidden[i][j].m_synapses.length; k++)
					{
						m_hidden[i][j].m_synapses[k] = new Synapse();
						
						m_hidden[i][j].m_synapses[k].m_node = m_hidden[i - 1][k];
						
						m_hidden[i][j].m_synapses[k].m_weight = Util.getRandom(minWeight, maxWeight);
					}
				}
			}
			
			//connect last hidden layer to output layer
			m_outputs = new Neuron[numOutputs];
			
			for (int i = 0; i < m_outputs.length; i++)
			{
				m_outputs[i] = new Neuron();
				
				m_outputs[i].m_synapses = new Synapse[numPerHidden];
				
				m_outputs[i].m_bias = Util.getRandom(minWeight, maxWeight);
				
				for (int j = 0; j < numPerHidden; j++)
				{
					m_outputs[i].m_synapses[j] = new Synapse();
					
					m_outputs[i].m_synapses[j].m_node = m_hidden[m_hidden.length - 1][j];
					
					m_outputs[i].m_synapses[j].m_weight = Util.getRandom(minWeight, maxWeight);
				}
			}
		}
		else
		{
			//connect output layer to input layer
			m_outputs = new Neuron[numOutputs];
			
			for (int i = 0; i < m_outputs.length; i++)
			{
				m_outputs[i] = new Neuron();
				
				m_outputs[i].m_synapses = new Synapse[m_inputs.length];
				
				for (int j = 0; j < m_inputs.length; j++)
				{
					m_outputs[i].m_synapses[j] = new Synapse();
					
					m_outputs[i].m_synapses[j].m_node = m_inputs[j];
				}
			}
		}
		
		m_numInputs = numInputs;
		
		m_numOutputs = numOutputs;
		
		m_numHidden = numHidden;
		
		m_numPerHidden = numPerHidden;
		
		m_minWeight = minWeight;
		
		m_maxWeight = maxWeight;
	}
	
	/**
	 * Constructs a FFNN from two parents. The new FFNN is the child.
	 * @param parentA
	 * @param parentB
	 * @param mutationRate Rate of mutation.
	 * @param maxPerturbation How much a weight can possibly change during mutation.
	 */
	public FFNN(FFNN parentA, FFNN parentB, float mutationRate, float maxPerturbation)
	{
		m_inputs = new Node[parentA.m_numInputs];
		
		for (int i = 0; i < m_inputs.length; i++)
			m_inputs[i] = new Node();
		
		if (parentA.m_numHidden > 0)
		{
			m_hidden = new Neuron[parentA.m_numHidden][parentA.m_numPerHidden];
			
			m_hidden[0] = new Neuron[parentA.m_numPerHidden];
			
			for (int i = 0; i < m_hidden[0].length; i++)
			{
				m_hidden[0][i] = new Neuron();
				
				m_hidden[0][i].m_synapses = new Synapse[parentA.m_numInputs];
				
				if (Math.random() < 0.5f)
					m_hidden[0][i].m_bias = parentA.m_hidden[0][i].m_bias;
				else
					m_hidden[0][i].m_bias = parentB.m_hidden[0][i].m_bias;
				
				if (Math.random() < mutationRate)
					m_hidden[0][i].m_bias += Util.getRandom(-maxPerturbation, maxPerturbation);
				
				for (int j = 0; j < m_hidden[0][i].m_synapses.length; j++)
				{
					m_hidden[0][i].m_synapses[j] = new Synapse();
					
					m_hidden[0][i].m_synapses[j].m_node = m_inputs[j];
					
					if (Math.random() < 0.5f)
						m_hidden[0][i].m_synapses[j].m_weight = parentA.m_hidden[0][i].m_synapses[j].m_weight;
					else
						m_hidden[0][i].m_synapses[j].m_weight = parentB.m_hidden[0][i].m_synapses[j].m_weight;
					
					if (Math.random() < mutationRate)
						m_hidden[0][i].m_synapses[j].m_weight += Util.getRandom(-maxPerturbation, maxPerturbation);
				}
			}
			
			for (int i = 1; i < m_hidden.length; i++)
			{
				m_hidden[i] = new Neuron[parentA.m_numPerHidden];
				
				for (int j = 0; j < m_hidden[i].length; j++)
				{
					m_hidden[i][j] = new Neuron();
					
					m_hidden[i][j].m_synapses = new Synapse[parentA.m_numPerHidden];
					
					if (Math.random() < 0.5f)
						m_hidden[i][j].m_bias = parentA.m_hidden[i][j].m_bias;
					else
						m_hidden[i][j].m_bias = parentB.m_hidden[i][j].m_bias;
					
					if (Math.random() < mutationRate)
						m_hidden[i][j].m_bias += Util.getRandom(-maxPerturbation, maxPerturbation);
					
					for (int k = 0; k < m_hidden[i][j].m_synapses.length; k++)
					{
						m_hidden[i][j].m_synapses[k] = new Synapse();
						
						m_hidden[i][j].m_synapses[k].m_node = m_hidden[i - 1][j];
						
						if (Math.random() < 0.5f)
							m_hidden[i][j].m_synapses[k].m_weight = parentA.m_hidden[i][j].m_synapses[k].m_weight;
						else
							m_hidden[i][j].m_synapses[k].m_weight = parentB.m_hidden[i][j].m_synapses[k].m_weight;
						
						if (Math.random() < mutationRate)
							m_hidden[i][j].m_synapses[k].m_weight += Util.getRandom(-maxPerturbation, maxPerturbation);
					}
				}
			}
			
			m_outputs = new Neuron[parentA.m_numOutputs];
			
			for (int i = 0; i < m_outputs.length; i++)
			{
				m_outputs[i] = new Neuron();
				
				m_outputs[i].m_synapses = new Synapse[parentA.m_numPerHidden];
				
				if (Math.random() < 0.5f)
					m_outputs[i].m_bias = parentA.m_outputs[i].m_bias;
				else
					m_outputs[i].m_bias = parentB.m_outputs[i].m_bias;
				
				if (Math.random() < mutationRate)
					m_outputs[i].m_bias += Util.getRandom(-maxPerturbation, maxPerturbation);
				
				for (int j = 0; j < m_outputs[i].m_synapses.length; j++)
				{
					m_outputs[i].m_synapses[j] = new Synapse();
					
					m_outputs[i].m_synapses[j].m_node = m_hidden[m_hidden.length - 1][i];
					
					if (Math.random() < 0.5f)
						m_outputs[i].m_synapses[j].m_weight = parentA.m_outputs[i].m_synapses[j].m_weight;
					else
						m_outputs[i].m_synapses[j].m_weight = parentB.m_outputs[i].m_synapses[j].m_weight;
					
					if (Math.random() < mutationRate)
						m_outputs[i].m_synapses[j].m_weight += Util.getRandom(-maxPerturbation, maxPerturbation);
				}
			}
		}
		else
		{
			m_outputs = new Neuron[parentA.m_numOutputs];
			
			for (int i = 0; i < m_outputs.length; i++)
			{
				m_outputs[i] = new Neuron();
				
				m_outputs[i].m_synapses = new Synapse[parentA.m_numInputs];
				
				if (Math.random() < 0.5f)
					m_outputs[i].m_bias = parentA.m_outputs[i].m_bias;
				else
					m_outputs[i].m_bias = parentB.m_outputs[i].m_bias;
				
				if (Math.random() < mutationRate)
					m_outputs[i].m_bias += Util.getRandom(-maxPerturbation, maxPerturbation);
				
				for (int j = 0; j < m_outputs[i].m_synapses.length; j++)
				{
					m_outputs[i].m_synapses[j] = new Synapse();
					
					m_outputs[i].m_synapses[j].m_node = m_inputs[i];
					
					if (Math.random() < 0.5f)
						m_outputs[i].m_synapses[j].m_weight = parentA.m_outputs[i].m_synapses[j].m_weight;
					else
						m_outputs[i].m_synapses[j].m_weight = parentB.m_outputs[i].m_synapses[j].m_weight;
					
					if (Math.random() < mutationRate)
						m_outputs[i].m_synapses[j].m_weight += Util.getRandom(-maxPerturbation, maxPerturbation);
				}
			}
		}
		
		m_numInputs = parentA.m_numInputs;
		
		m_numOutputs = parentA.m_numOutputs;
		
		m_numHidden = parentA.m_numHidden;
		
		m_numPerHidden = parentA.m_numPerHidden;
		
		m_minWeight = parentA.m_minWeight;
		
		m_maxWeight = parentA.m_maxWeight;
	}
	
	/**
	 * Constructs a FFNN from a file.
	 * @param filePath
	 */
	public FFNN(String filePath)
	{
		int numInputs = 0;
		int numOutputs = 0;
		
		int numHidden = 0;
		int numPerHidden = 0;
		
		float minWeight = -1.0f;
		float maxWeight = 1.0f;
		
		float[] firstHiddenBias = null;
		float[] firstHiddenWeights = null;
		
		float[][] hiddenWeights = null;
		float[][] hiddenBias = null;
		
		float[] outputWeights = null;
		float[] outputBias = null;
		
		try
		{
			BufferedReader in = new BufferedReader(new FileReader(filePath));
			
			String line = "";
			
			if ((line = in.readLine()) != null)
			{
				String[] split = line.split(" ");
				
				if (split.length != 6)
					System.err.println("Error in line [1] of " + filePath);
				
				numInputs = Integer.parseInt(split[0]);
				numOutputs = Integer.parseInt(split[1]);
				
				numHidden = Integer.parseInt(split[2]);
				numPerHidden = Integer.parseInt(split[3]);
				
				minWeight = Float.parseFloat(split[4]);
				maxWeight = Float.parseFloat(split[5]);
				
				firstHiddenBias = new float[numPerHidden];
				firstHiddenWeights = new float[numInputs * numPerHidden];
				
				hiddenWeights = new float[numHidden - 1][numPerHidden];
				hiddenBias = new float[numHidden - 1][numPerHidden];
				
				outputWeights = new float[numOutputs * numPerHidden];
				outputBias = new float[numOutputs];
			}
			else
				System.err.println("Error: line [1] does not exist.");
			
			if ((line = in.readLine()) != null)
			{
				if (line.contains(","))
				{
					String[] nodes = line.split(",");
					
					if (nodes.length != numOutputs)
						System.err.println("Error in line [2] of " + filePath);
					
					for (int i = 0; i < nodes.length; i++)
					{
						String[] split = nodes[i].split(" ");
						
						outputBias[i] = Float.parseFloat(split[0]);
						
						for (int j = 1; j < split.length; j++)
							outputWeights[i * numPerHidden + j - 1] = Float.parseFloat(split[j]);
					}
				}
				else
				{
					String[] split = line.split(" ");
					
					outputBias[0] = Float.parseFloat(split[0]);
					
					for (int j = 1; j < split.length; j++)
						outputWeights[j - 1] = Float.parseFloat(split[j]);
				}
			}
			else
				System.err.println("Error: line [2] does not exist.");
			
			if (numHidden > 0)
			{
				if ((line = in.readLine()) != null)
				{
					if (line.contains(","))
					{
						String[] nodes = line.split(",");
						
						if (nodes.length != numPerHidden)
							System.err.println("Error in line [3] of " + filePath);
						
						for (int i = 0; i < nodes.length; i++)
						{
							String[] split = nodes[i].split(" ");
							
							firstHiddenBias[i] = Float.parseFloat(split[0]);
							
							for (int j = 1; j < split.length; j++)
								firstHiddenWeights[i * numInputs + j - 1] = Float.parseFloat(split[j]);
						}
					}
					else
					{
						String[] split = line.split(" ");
						
						firstHiddenBias[0] = Float.parseFloat(split[0]);
						
						for (int j = 1; j < split.length; j++)
							firstHiddenWeights[j - 1] = Float.parseFloat(split[j]);
					}
				}
				else
					System.err.println("Error: line [3] does not exist.");
				
				int lineNum = 4;
				
				while ((line = in.readLine()) != null)
				{
					if (line.contains(","))
					{
						String[] nodes = line.split(",");
						
						if (nodes.length != numPerHidden)
							System.err.println("Error in line [" + lineNum + "] of " + filePath);
						
						for (int i = 0; i < nodes.length; i++)
						{
							String[] split = nodes[i].split(" ");
							
							hiddenBias[lineNum - 4][i] = Float.parseFloat(split[0]);
							
							for (int j = 1; j < split.length; j++)
								hiddenWeights[lineNum - 4][i * numPerHidden + j - 1] = Float.parseFloat(split[j]);
						}
					}
					else
					{
						String[] split = line.split(" ");
						
						hiddenBias[lineNum - 4][0] = Float.parseFloat(split[0]);
						
						for (int j = 1; j < split.length; j++)
							hiddenWeights[lineNum - 4][j - 1] = Float.parseFloat(split[j]);
					}
					
					lineNum++;
				}
			}
			
			in.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		//construction
		
		m_inputs = new Node[numInputs];
		
		for (int i = 0; i < m_inputs.length; i++)
			m_inputs[i] = new Node();
		
		if (numHidden > 0)
		{
			m_hidden = new Neuron[numHidden][numPerHidden];
			
			//first layer connected to input layer
			m_hidden[0] = new Neuron[numPerHidden];
			
			for (int i = 0; i < m_hidden[0].length; i++)
			{
				m_hidden[0][i] = new Neuron();
				
				m_hidden[0][i].m_synapses = new Synapse[numInputs];
				
				m_hidden[0][i].m_bias = firstHiddenBias[i];
				
				for (int j = 0; j < m_hidden[0][i].m_synapses.length; j++)
				{
					m_hidden[0][i].m_synapses[j] = new Synapse();
					
					m_hidden[0][i].m_synapses[j].m_node = m_inputs[j];
					
					m_hidden[0][i].m_synapses[j].m_weight = firstHiddenWeights[i * numInputs + j];
				}
			}
			
			//in-between hidden layers
			for (int i = 1; i < m_hidden.length; i++)
			{
				m_hidden[i] = new Neuron[numPerHidden];
				
				for (int j = 0; j < m_hidden[i].length; j++)
				{
					m_hidden[i][j] = new Neuron();
					
					m_hidden[i][j].m_synapses = new Synapse[m_hidden[i - 1].length];
					
					m_hidden[i][j].m_bias = hiddenBias[i - 1][j];
					
					for (int k = 0; k < m_hidden[i][j].m_synapses.length; k++)
					{
						m_hidden[i][j].m_synapses[k] = new Synapse();
						
						m_hidden[i][j].m_synapses[k].m_node = m_hidden[i - 1][k];
						
						m_hidden[i][j].m_synapses[k].m_weight = hiddenWeights[i - 1][j * numPerHidden + k];
					}
				}
			}
			
			//connect last hidden layer to output layer
			m_outputs = new Neuron[numOutputs];
			
			for (int i = 0; i < m_outputs.length; i++)
			{
				m_outputs[i] = new Neuron();
				
				m_outputs[i].m_synapses = new Synapse[numPerHidden];
				
				m_outputs[i].m_bias = outputBias[i];
				
				for (int j = 0; j < numPerHidden; j++)
				{
					m_outputs[i].m_synapses[j] = new Synapse();
					
					m_outputs[i].m_synapses[j].m_node = m_hidden[m_hidden.length - 1][j];
					
					m_outputs[i].m_synapses[j].m_weight = outputWeights[i * numPerHidden + j];
				}
			}
		}
		else
		{
			//connect output layer to input layer
			m_outputs = new Neuron[numOutputs];
			
			for (int i = 0; i < m_outputs.length; i++)
			{
				m_outputs[i] = new Neuron();
				
				m_outputs[i].m_synapses = new Synapse[m_inputs.length];
				
				for (int j = 0; j < m_inputs.length; j++)
				{
					m_outputs[i].m_synapses[j] = new Synapse();
					
					m_outputs[i].m_synapses[j].m_node = m_inputs[j];
				}
			}
		}
		
		m_numInputs = numInputs;
		
		m_numOutputs = numOutputs;
		
		m_numHidden = numHidden;
		
		m_numPerHidden = numPerHidden;
		
		m_minWeight = minWeight;
		
		m_maxWeight = maxWeight;
	}
	
	/**
	 * Updates the Neural Network.
	 */
	public void update()
	{
		for (int i = 0; i < m_hidden.length; i++)
			for (int k = 0; k < m_hidden[i].length; k++)
				m_hidden[i][k].update(m_activationMultiplier);
		
		for (int i = 0; i < m_outputs.length; i++)
			m_outputs[i].update(m_activationMultiplier);
	}
	
	/**
	 * Updates the Neural Network with linear output.
	 */
	public void updateLinear()
	{
		for (int i = 0; i < m_hidden.length; i++)
			for (int j = 0; j < m_hidden[i].length; j++)
				m_hidden[i][j].update(m_activationMultiplier);
		
		for (int i = 0; i < m_outputs.length; i++)
			m_outputs[i].updateLinear(m_activationMultiplier);
	}
	
	/**
	 * Calculates the error.
	 * @param targetOutputs The target outputs.
	 */
	public void calculateError(float[] targetOutputs)
	{
		//error = difference of target and actual times the derivative of the function
		for (int i = 0; i < m_outputs.length; i++)
			m_outputs[i].m_error = (targetOutputs[i] - m_outputs[i].m_output) * m_outputs[i].m_output * (1.0f - m_outputs[i].m_output);
		
		for (int i = 0; i < m_hidden[m_hidden.length - 1].length; i++)
		{
			float sum = 0.0f;
			
			for (int j = 0; j < m_outputs.length; j++)
				sum += m_outputs[j].m_synapses[i].m_weight * m_outputs[j].m_error;
			
			m_hidden[m_hidden.length - 1][i].m_error = m_hidden[m_hidden.length - 1][i].m_output * (1.0f - m_hidden[m_hidden.length - 1][i].m_output) * sum;
		}
		
		for (int l = m_hidden.length - 2; l >= 0; l--)
		{
			for (int i = 0; i < m_hidden[l].length; i++)
			{
				float sum = 0.0f;
				
				for (int j = 0; j < m_hidden[l + 1].length; j++)
					sum += m_hidden[l + 1][j].m_synapses[i].m_weight * m_hidden[l + 1][j].m_error;
				
				m_hidden[l][i].m_error = m_hidden[l][i].m_output * (1.0f - m_hidden[l][i].m_output) * sum;
			}
		}
	}
	
	/**
	 * Calculates the error with linear output.
	 * @param targetOutputs The target outputs.
	 */
	public void calculateErrorWithLinearOutput(float[] targetOutputs)
	{
		//error = difference of target and actual
		for (int i = 0; i < m_outputs.length; i++)
			m_outputs[i].m_error = targetOutputs[i] - m_outputs[i].m_output;
		
		for (int i = 0; i < m_hidden[m_hidden.length - 1].length; i++)
		{
			float sum = 0.0f;
			
			for (int j = 0; j < m_outputs.length; j++)
				sum += m_outputs[j].m_synapses[i].m_weight * m_outputs[j].m_error;
			
			m_hidden[m_hidden.length - 1][i].m_error = m_hidden[m_hidden.length - 1][i].m_output * (1.0f - m_hidden[m_hidden.length - 1][i].m_output) * sum;
		}
		
		for (int l = m_hidden.length - 2; l >= 0; l--)
		{
			for (int i = 0; i < m_hidden[l].length; i++)
			{
				float sum = 0.0f;
				
				for (int j = 0; j < m_hidden[l + 1].length; j++)
					sum += m_hidden[l + 1][j].m_synapses[i].m_weight * m_hidden[l + 1][j].m_error;
				
				m_hidden[l][i].m_error = m_hidden[l][i].m_output * (1.0f - m_hidden[l][i].m_output) * sum;
			}
		}
	}
	
	/**
	 * Adjusts the weight of the synapses and bias of the neurons.
	 * @param learningRate The rate of learning.
	 */
	public void moveAlongGradient(float learningRate)
	{
		for (int i = 0; i < m_outputs.length; i++)
		{
			for (int j = 0; j < m_outputs[i].m_synapses.length; j++)
			{
				float dWeight = m_outputs[i].m_error * m_outputs[i].m_synapses[j].m_node.m_output * learningRate + m_outputs[i].m_synapses[j].m_prevDWeight * m_momentum;
				
				m_outputs[i].m_synapses[j].m_weight += dWeight;
				
				m_outputs[i].m_synapses[j].m_prevDWeight = dWeight;
			}
			
			float dBias = m_outputs[i].m_error * learningRate + m_outputs[i].m_prevDBias * m_momentum;
			
			m_outputs[i].m_bias += dBias;
			
			m_outputs[i].m_prevDBias = dBias;
		}
		
		for (int l = 0; l < m_hidden.length; l++)
		{
			for (int i = 0; i < m_hidden[l].length; i++)
			{
				for (int j = 0; j < m_hidden[l][i].m_synapses.length; j++)
				{
					float dWeight = m_hidden[l][i].m_error * m_hidden[l][i].m_synapses[j].m_node.m_output * learningRate + m_hidden[l][i].m_synapses[j].m_prevDWeight * m_momentum;
					
					m_hidden[l][i].m_synapses[j].m_weight += dWeight;
					
					m_hidden[l][i].m_synapses[j].m_prevDWeight = dWeight;
				}
				
				float dBias = m_hidden[l][i].m_error * learningRate + m_hidden[l][i].m_prevDBias * m_momentum;
				
				m_hidden[l][i].m_bias += dBias;
				
				m_hidden[l][i].m_prevDBias = dBias;
			}
		}
	}
	
	/**
	 * Saves this FFNN to a file.
	 * @param filePath
	 */
	public void save(String filePath)
	{
		try
		{
			BufferedWriter out = new BufferedWriter(new FileWriter(filePath));
			
			if (m_hidden.length > 0)
			{
				out.write("" + m_inputs.length + " " + m_outputs.length + " " + m_hidden.length + " " + m_hidden[0].length + " " + m_minWeight + " " + m_maxWeight + "\n");
				
				for (int i = 0; i < m_outputs.length - 1; i++)
					out.write(m_outputs[i].toString() + ",");
				
				out.write(m_outputs[m_outputs.length - 1].toString() + "\n");
				
				for (int i = 0; i < m_hidden.length; i++)
				{
					for (int j = 0; j < m_hidden[i].length- 1; j++)
						out.write(m_hidden[i][j].toString() + ",");
					
					out.write(m_hidden[i][m_hidden[i].length - 1].toString() + "\n");
				}
			}
			else
			{
				out.write("" + m_inputs.length + " " + m_outputs.length + " " + 0 + " " + 0 + " " + m_minWeight + " " + m_maxWeight + "\n");
				
				for (int i = 0; i < m_outputs.length - 1; i++)
					out.write(m_outputs[i].toString() + ",");
				
				out.write(m_outputs[m_outputs.length - 1].toString() + "\n");
			}
			
			out.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void setInput(int index, float value)
	{
		m_inputs[index].m_output = value;
	}
	
	public float getInput(int index)
	{
		return m_inputs[index].m_output;
	}
	
	public float getOutput(int index)
	{
		return m_outputs[index].m_output;
	}
	
	public int getNumInputs()
	{
		return m_inputs.length;
	}
	
	public int getNumOutputs()
	{
		return m_outputs.length;
	}
}
