package MultiAgent;

import NeuralNetwork.FFNN;
import Util.Util;

public class GeneticAlgorithm
{
	private FFNN[] m_agents;
	
	public float[] m_rewards;
	
	/**
	 * Constructor.
	 * @param numAgents
	 * @param numInputs
	 * @param numOutputs
	 * @param numHidden
	 * @param numPerHidden
	 * @param minWeight
	 * @param maxWeight
	 */
	public GeneticAlgorithm(int numAgents, int numInputs, int numOutputs, int numHidden, int numPerHidden, float minWeight, float maxWeight)
	{
		m_agents = new FFNN[numAgents];
		
		for (int i = 0; i < numAgents; i++)
			m_agents[i] = new FFNN(numInputs, numOutputs, numHidden, numPerHidden, minWeight, maxWeight);
		
		m_rewards = new float[numAgents];
		
		for (int i = 0; i < numAgents; i++)
			m_rewards[i] = 0.0f;
	}
	
	/**
	 * Evolves the agents.
	 * @param selectionGreedExponent Selection greed exponent.
	 * @param mutationRate Rate of mutation.
	 * @param maxPerturbation How much a weight can possibly change during mutation.
	 */
	public void evolve(float selectionGreedExponent, float mutationRate, float maxPerturbation)
	{
		FFNN[] newAgents = new FFNN[m_agents.length];
		
		for (int i = 0; i < newAgents.length; i++)
		{
			int selectionA = Util.rouletteWheelSelection(m_rewards, selectionGreedExponent);
			
			int selectionB = Util.rouletteWheelSelection(m_rewards, selectionGreedExponent);
			
			newAgents[i] = new FFNN(m_agents[selectionA], m_agents[selectionB], mutationRate, maxPerturbation);
		}
		
		m_agents = newAgents;
	}
	
	public int getNumAgents()
	{
		return m_agents.length;
	}
	
	public FFNN getAgent(int index)
	{
		return m_agents[index];
	}
	
	public int gethighestRewardIndex()
	{
		float highest = m_rewards[0];
		
		int highestIndex = 0;
		
		for (int i = 0; i < m_rewards.length; i++)
			if (m_rewards[i] > highest)
			{
				highest = m_rewards[i];
				
				highestIndex = i;
			}
		
		return highestIndex;
	}
}
