package SingleAgent;

import java.util.LinkedList;

import NeuralNetwork.FFNN;
import Util.Util;

public class Cacla
{
	private FFNN m_actor;
	
	private FFNN m_critic;
	
	private float[] m_prevInputs;
	
	private float[] m_actualOutputs;
	
	private LinkedList<CriticIOSet> m_criticIOSetChain;
	
	/**
	 * Number of actor rehearsals.
	 */
	public int m_actorRehearsalCount = 200;
	
	/**
	 * The chance to train on a new sample.
	 */
	public float m_trainNewSampleChance = 0.5f;
	
	/**
	 * How much it values future states.
	 */
	public float m_gamma = 0.9f;
	
	/**
	 * How quickly you want to update to the new value.
	 */
	public float m_lambda = 0.1f;
	
	/**
	 * Number of back propagations for the actor.
	 */
	public int m_numBackPropPassesActor = 500;
	
	/**
	 * Number of back propagations for the critic.
	 */
	public int m_numBackPropPassesCritic = 500;
	
	/**
	 * Learning rate of the actor.
	 */
	public float m_learningRateActor = 0.001f;
	
	/**
	 * Learning rate of the critic.
	 */
	public float m_learningRateCritic = 0.001f;
	
	/**
	 * Standard deviation of normal distribution.
	 */
	public float m_standardDeviation = 0.3f;
	
	/**
	 * The maximum IO chain size for the critic.
	 */
	public int m_maxCriticIOSetChainSize = 1000;
	
	/**
	 * Constructor.
	 * @param numInputs
	 * @param numOutputs
	 * @param numHiddenActor
	 * @param numPerHiddenActor
	 * @param numHiddenCritic
	 * @param numPerHiddenCritic
	 * @param minWeight
	 * @param maxWeight
	 */
	public Cacla(int numInputs, int numOutputs, int numHiddenActor, int numPerHiddenActor, int numHiddenCritic, int numPerHiddenCritic, float minWeight, float maxWeight)
	{
		m_actor = new FFNN(numInputs, numOutputs, numHiddenActor, numPerHiddenActor, minWeight, maxWeight);
		
		m_critic = new FFNN(numInputs, 1, numHiddenCritic, numPerHiddenCritic, minWeight, maxWeight);
		
		m_prevInputs = new float[numInputs];
		
		for (int i = 0; i < m_prevInputs.length; i++)
			m_prevInputs[i] = 0.0f;
		
		m_actualOutputs = new float[numOutputs];
		
		m_criticIOSetChain = new LinkedList<CriticIOSet>();
	}
	
	/**
	 * Updates the actor-critic with the given reward.
	 * @param reward
	 */
	public void update(float reward)
	{
		ActorIOSet[] actorRehearsalArray = new ActorIOSet[m_actorRehearsalCount];
		
		for (int i = 0; i < m_actorRehearsalCount; i++)
		{
			ActorIOSet set = new ActorIOSet();
			
			set.m_inputs = new float[m_actor.getNumInputs()];
			
			for (int k = 0; k < m_actor.getNumInputs(); k++)
			{
				set.m_inputs[k] = (float)Math.random();
				
				m_actor.setInput(k, set.m_inputs[k]);
			}
			
			m_actor.update();
			
			set.m_outputs = new float[m_actor.getNumOutputs()];
			
			for (int k = 0; k < m_actor.getNumOutputs(); k++)
				set.m_outputs[k] = m_actor.getOutput(k);
			
			actorRehearsalArray[i] = set;
		}
		
		float[] currentInputs = new float[m_critic.getNumInputs()];
		
		for (int i = 0; i < currentInputs.length; i++)
			currentInputs[i] = m_critic.getInput(i);
		
		m_critic.updateLinear();
		
		float value = m_critic.getOutput(0);
		
		for (int i = 0; i < m_critic.getNumInputs(); i++)
			m_critic.setInput(i, m_prevInputs[i]);
		
		m_critic.updateLinear();
		
		float prevValue = m_critic.getOutput(0);
		
		float newPrevValue = reward + m_gamma * value;
		
		float error = newPrevValue - prevValue;
		
		float prevChainValue = newPrevValue;
		
		for (CriticIOSet set : m_criticIOSetChain)
		{
			set.m_value = (1.0f - m_lambda) * set.m_value + m_lambda * (set.m_reward + m_gamma * prevChainValue);
			
			prevChainValue = set.m_value;
		}
		
		CriticIOSet newSet = new CriticIOSet();
		
		newSet.m_inputs = m_prevInputs.clone();
		
		newSet.m_reward = reward;
		
		newSet.m_value = newPrevValue;
		
		m_criticIOSetChain.addFirst(newSet);
		
		while (m_criticIOSetChain.size() > m_maxCriticIOSetChainSize)
			m_criticIOSetChain.removeLast();
		
		CriticIOSet[] criticIOSetChainArray = new CriticIOSet[m_criticIOSetChain.size()];
		
		for (int i = 0; i < criticIOSetChainArray.length; i++)
			criticIOSetChainArray[i] = m_criticIOSetChain.get(i);
		
		for (int i = 0; i < m_numBackPropPassesCritic; i++)
		{
			int rand = (int)(Math.random() * criticIOSetChainArray.length);
			
			CriticIOSet set = criticIOSetChainArray[rand];
			
			for (int k = 0; k < m_critic.getNumInputs(); k++)
				m_critic.setInput(k, set.m_inputs[k]);
			
			m_critic.updateLinear();
			
			m_critic.calculateErrorWithLinearOutput(new float[] {set.m_value});
			
			m_critic.moveAlongGradient(m_learningRateCritic);
		}
		
		if (error > 0.0f && m_standardDeviation > 0.0f)
		{
			for (int i = 0; i < m_numBackPropPassesActor; i++)
			{
				if ((float)Math.random() < m_trainNewSampleChance)
				{
					for (int k = 0; k < m_prevInputs.length; k++)
						m_actor.setInput(k, m_prevInputs[k]);
					
					m_actor.update();
					
					m_actor.calculateError(m_actualOutputs);
					
					m_actor.moveAlongGradient(m_learningRateActor);
				}
				else
				{
					int sampleIndex = (int)(Math.random() * actorRehearsalArray.length);
					
					for (int k = 0; k < actorRehearsalArray[sampleIndex].m_inputs.length; k++)
						m_actor.setInput(k, actorRehearsalArray[sampleIndex].m_inputs[k]);
					
					m_actor.update();
					
					m_actor.calculateError(actorRehearsalArray[sampleIndex].m_outputs);
					
					m_actor.moveAlongGradient(m_learningRateActor);
				}
			}
		}
		
		for (int k = 0; k < currentInputs.length; k++)
			m_actor.setInput(k, currentInputs[k]);
		
		m_actor.update();
		
		for (int i = 0; i < m_actualOutputs.length; i++)
			m_actualOutputs[i] = m_actor.getOutput(i) + Util.getGaussian(0.0f, m_standardDeviation);
		
		m_prevInputs = currentInputs.clone();
	}
	
	public void setInput(int index, float value)
	{
		m_actor.setInput(index, value);
		
		m_critic.setInput(index, value);
	}
	
	public float getOutput(int index)
	{
		return m_actualOutputs[index];
	}
	
	class CriticIOSet
	{	
		public float[] m_inputs;
		
		public float m_reward;
		
		public float m_value;
	}
	
	class ActorIOSet
	{
		public float[] m_inputs;
		
		public float[] m_outputs;
	}
}
