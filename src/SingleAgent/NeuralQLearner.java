package SingleAgent;

import NeuralNetwork.FFNN;

public class NeuralQLearner
{
	private int m_numInputs;
	
	private int m_numOutputs;
	
	private FFNN m_qNet;
	
	private float[] m_currentInputs;
	
	private float[] m_prevInputs;
	
	private int m_outputIndex = 0;
	
	private float m_prevQValue = 0.0f;
	
	/**
	 * The number of back propagation passes.
	 */
	public int m_numBackPropPasses = 1;
	
	/**
	 * How much to diminish value of current state.
	 */
	public float m_gamma = 0.95f;
	
	/**
	 * Learning rate of update.
	 */
	public float m_alpha = 0.01f;
	
	/**
	 * Learning rate during back propagation.
	 */
	public float m_beta = 0.01f;
	
	/**
	 * Randomness to choose a random action instead of best.
	 */
	public float m_epsilon = 0.25f;
	
	/**
	 * Constructor.
	 * @param numInputs Number of inputs.
	 * @param numOutputs Number of outputs.
	 * @param numHidden Number of hidden layers.
	 * @param numPerHidden Number of neurons per hidden layer.
	 * @param minWeight Minimum possible weight during construction of synapses.
	 * @param maxWeight Maximum possible weight during construction of synapses.
	 */
	public NeuralQLearner(int numInputs, int numOutputs, int numHidden, int numPerHidden, float minWeight, float maxWeight)
	{
		m_numInputs = numInputs;
		
		m_numOutputs = numOutputs;
		
		m_qNet = new FFNN(numInputs + numOutputs, 1, numHidden, numPerHidden, minWeight, maxWeight);
		
		m_currentInputs = new float[numInputs];
		
		m_prevInputs = new float[numInputs];
		
		for (int i = 0; i < numInputs; i++)
		{
			m_currentInputs[i] = 0.0f;
			
			m_prevInputs[i] = 0.0f;
		}
	}
	
	/**
	 * Updates the Neural Q Learner with the given reward value.
	 * @param reward
	 */
	public void update(float reward)
	{
		int prevOutputIndex = m_outputIndex;
		
		for (int i = 0; i < m_numInputs; i++)
			m_qNet.setInput(i, m_currentInputs[i]);
		
		int highestOutputIndex = 0;
		
		float highest = Float.NEGATIVE_INFINITY;
		
		float[] qValues = new float[m_numOutputs];
		
		for (int i = 0; i < m_numOutputs; i++)
		{
			for (int k = 0; k < m_numOutputs; k++)
				m_qNet.setInput(m_numInputs + k, 0.0f);
			
			m_qNet.setInput(m_numInputs + i, 1.0f);
			
			m_qNet.updateLinear();
			
			qValues[i] = m_qNet.getOutput(0);
			
			if (m_qNet.getOutput(0) > highest)
			{
				highest = m_qNet.getOutput(0);
				
				highestOutputIndex = i;
			}
		}
		
		if ((float)Math.random() < m_epsilon)
			m_outputIndex = (int)(Math.random() * m_numOutputs);
		else
			m_outputIndex = highestOutputIndex;
		
		float newPrevQValue = m_prevQValue + m_alpha * (reward + m_gamma * qValues[m_outputIndex] - m_prevQValue);
		
		m_prevQValue = qValues[m_outputIndex];
		
		if (m_epsilon > 0.0f)
		{
			for (int i = 0; i < m_numBackPropPasses; i++)
			{
				for (int k = 0; k < m_numInputs; k++)
					m_qNet.setInput(k, m_prevInputs[k]);
				
				for (int k = 0; k < m_numOutputs; k++)
					m_qNet.setInput(m_numInputs + k, 0.0f);
				
				m_qNet.setInput(m_numInputs + prevOutputIndex, 1.0f);
				
				m_qNet.updateLinear();
				
				m_qNet.calculateErrorWithLinearOutput(new float[]{newPrevQValue});
				
				m_qNet.moveAlongGradient(m_beta);
			}
		}
		
		m_prevInputs = m_currentInputs.clone();
	}
	
	public void setInput(int index, float value)
	{
		m_currentInputs[index] = value;
	}
	
	public int getOutput()
	{
		return m_outputIndex;
	}
	
	class IOSet
	{
		public float[] m_inputs;
		
		public float m_reward;
		
		public float m_qValue;
	}
}
