package Util;

import java.util.Random;

public class Util
{
	private static Random m_random = new Random();
	
	/**
	 * Gets a random number between the two floats.
	 * @param min
	 * @param max
	 * @return Random value between min and max.
	 */
	public static float getRandom(float min, float max)
	{
		return (float)(Math.random() * (max - min)) + min;
	}
	
	/**
	 * Gets a random value from a Gaussian distribution with the given mean and standard deviation.
	 * @param mean
	 * @param standardDeviation
	 * @return A random value from a Gaussian distribution
	 */
	public static float getGaussian(float mean, float standardDeviation)
	{
		return mean + (float)m_random.nextGaussian() * standardDeviation;
	}
	
	/**
	 * Selects a random index using the roulette wheel selection algorithm.
	 * @param values
	 * @param greedExponent
	 * @return A randomly selected index.
	 */
	public static int rouletteWheelSelection(float[] values, float greedExponent)
	{
		float[] copy = new float[values.length];
		
		for (int i = 0; i < copy.length; i++)
			copy[i] = values[i];
		
		float lowest = copy[0];
		
		for (int i = 0; i < copy.length; i++)
			if (copy[i] < lowest)
				lowest = copy[i];
		
		for (int i = 0; i < copy.length; i++)
		{
			copy[i] -= lowest;
			
			copy[i] = (float)Math.pow(copy[i], greedExponent);
		}
		
		float sum = 0.0f;
		
		for (int i = 0; i < copy.length; i++)
			sum += copy[i];
		
		float randNum = (float)(Math.random() * sum);
		
	    float current = 0.0f;
	    
		for (int i = 0; i < copy.length; i++)
		{
			current += copy[i];
			
			if (current >= randNum)
				return i;
		}
		
		return 0;
	}
}
